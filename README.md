# Users API

## Features
API supports:
* adding user
* deleting user
* getting single user
* getting paginated list of users

Every request is logged along with its unique request id which is also visible in all logs produced in context of the particular request.

## TODOs

* update user operation
* RPC
* definitely more unit tests
* integration tests

## How to start application ##
* By Makefile
```shell
make run
```
* Directly by using docker-compose
```shell
docker run
```

When application is running it will be available under [http://localhost:8080](http://localhost:8080)

To open the API documentation go to [http://localhost:8081](http://localhost:8081)

## Unit tests
* By Makefile
```shell
make unit-tests
```
* Directly by using docker-compose
```shell
docker-compose run http_api go test -v ./...
```
