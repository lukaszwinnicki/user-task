SHELL=/bin/sh
.SHELLFLAGS = -e -c

run:
	docker-compose up

lint:
	revive -config revive.toml ./...

unit-tests:
	docker-compose run http_api go test -v ./...
