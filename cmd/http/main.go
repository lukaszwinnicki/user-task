package main

import (
	"context"
	"fmt"
	nativeLog "log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog/hlog"

	"gitlab.com/lukaszwinnicki/user-task/pkg/clock"
	"gitlab.com/lukaszwinnicki/user-task/pkg/config"
	"gitlab.com/lukaszwinnicki/user-task/pkg/id"
	"gitlab.com/lukaszwinnicki/user-task/pkg/log"
	"gitlab.com/lukaszwinnicki/user-task/pkg/mysql"
	"gitlab.com/lukaszwinnicki/user-task/pkg/rabbit"
	"gitlab.com/lukaszwinnicki/user-task/pkg/users"
)

func main() {
	conf, err := config.GetConfigurationFromEnv()
	if err != nil {
		nativeLog.Fatal("can't get configuration", err)
	}

	logger, err := log.ConfigureLogger(conf.LogLevel)
	if err != nil {
		nativeLog.Fatal("can't prepare logger", err)
	}
	ctx := logger.WithContext(context.Background())

	dbConn, err := mysql.NewConnection(mysql.Configuration{
		Addr:            conf.Mysql.Addr,
		DB:              conf.Mysql.Db,
		User:            conf.Mysql.User,
		Password:        conf.Mysql.Password,
		MaxOpenConns:    conf.Mysql.MaxOpenConns,
		MaxIdleConns:    conf.Mysql.MaxIdleConns,
		ConnMaxLifetime: conf.Mysql.ConnMaxLifetime,
	})
	if err != nil {
		logger.Fatal().Err(err).Msg("can't initialize DB connection")
	}
	repo, err := mysql.NewUserRepository(ctx, dbConn, conf.Mysql.InitDB)
	if err != nil {
		logger.Fatal().Err(err).Msg("can't initialize users repo")
	}
	eventListener := rabbit.NewEventsListener()
	idGenerator := id.NewGenerator()
	cl := clock.NewClock()
	factory := users.NewUserFactory(idGenerator, cl)

	getUserByIDHandler := users.NewGetUserByIDQueryHandler(repo)
	getUsersHandler := users.NewGetUsersQueryHandler(repo)
	createUserHandler := users.NewCreateUserCommandHandler(repo, repo, factory, eventListener)
	deleteUserHandler := users.NewDeleteUserCommandHandler(repo, repo, eventListener)

	router := mux.NewRouter()
	router.Use(hlog.NewHandler(logger))
	router.Use(log.MiddlewareDecorateLogger(idGenerator))
	router.Use(log.MiddlewareLogRequest())

	router.HandleFunc("/users/{id}", NewGetUserByIDHTTPHandler(getUserByIDHandler)).Methods(http.MethodGet)
	router.HandleFunc("/users/{id}", NewDeleteUserHTTPHandler(deleteUserHandler)).Methods(http.MethodDelete)
	router.HandleFunc("/users", NewGetUsersHTTPHandler(getUsersHandler)).Methods(http.MethodGet)
	router.HandleFunc("/users", NewCreateUserHTTPHandler(createUserHandler)).Methods(http.MethodPost)

	router.HandleFunc("/health", NewHealthHTTPHandler()).Methods(http.MethodGet)

	addr := fmt.Sprintf("0.0.0.0:%d", conf.Port)

	srv := &http.Server{
		Handler:      router,
		Addr:         addr,
		WriteTimeout: 2 * time.Second,
		ReadTimeout:  2 * time.Second,
	}

	logger.Info().Str("addr", addr).Msg("started http server")
	err = srv.ListenAndServe()
	if err != nil {
		logger.Fatal().Err(err).Str("addr", addr).Msg("couldn't start http server")
	}
}
