package main

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/hlog"

	"gitlab.com/lukaszwinnicki/user-task/pkg/users"
)

type ErrorResp struct {
	Type    string `json:"type"`
	Message string `json:"message"`
}

type userResponse struct {
	ID        string `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Nickname  string `json:"nickname"`
	Password  string `json:"password"`
	Email     string `json:"email"`
	Country   string `json:"country"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

func mapUserToResponse(user users.User) userResponse {
	return userResponse{
		ID:        user.ID,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Nickname:  user.Nickname,
		Password:  user.Password,
		Email:     user.Email,
		Country:   user.Country,
		CreatedAt: user.CreatedAt.Format(time.RFC3339),
		UpdatedAt: user.UpdatedAt.Format(time.RFC3339),
	}
}

func produceInternalServerError(ctx context.Context, w http.ResponseWriter) {
	produceResponse(ctx, w, http.StatusInternalServerError, ErrorResp{
		Type:    "internal_server_error",
		Message: "Internal Server Error",
	})
}

func produceResponse(ctx context.Context, w http.ResponseWriter, statusCode int, resp any) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	jsonRes, err := json.Marshal(resp)
	if err != nil {
		zerolog.Ctx(ctx).
			Error().
			Err(err).
			Interface("resp", resp).
			Msg("something wrong during response unmarshalling")
		return
	}
	w.Write(jsonRes)
}

func NewHealthHTTPHandler() http.HandlerFunc {
	type response struct {
		Healthy bool `json:"healthy"`
	}
	return func(w http.ResponseWriter, req *http.Request) {
		produceResponse(req.Context(), w, http.StatusOK, response{Healthy: true})
		return
	}
}

func NewDeleteUserHTTPHandler(handler *users.DeleteUserCommandHandler) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		ctx := req.Context()
		vars := mux.Vars(req)
		userID, ok := vars["id"]
		if !ok {
			hlog.FromRequest(req).Error().Str("uri", req.RequestURI).Msg("missing user id in request")
			produceInternalServerError(ctx, w)
			return
		}
		err := handler.Handle(ctx, users.DeleteUserCommand{UserID: userID})
		if err != nil {
			hlog.FromRequest(req).Error().Err(err).Str("user_id", userID).Msg("error during deleting user")
			produceInternalServerError(ctx, w)
			return
		}

		w.WriteHeader(http.StatusNoContent)
	}
}

func NewGetUsersHTTPHandler(handler *users.GetUsersQueryHandler) http.HandlerFunc {
	type response struct {
		HasMore    bool           `json:"has_more"`
		NextCursor string         `json:"next_cursor,omitempty"`
		Items      []userResponse `json:"items"`
	}

	return func(w http.ResponseWriter, req *http.Request) {
		ctx := req.Context()

		httpQuery := req.URL.Query()
		query := users.GetUsersQuery{}
		if httpQuery.Has("start_cursor") {
			query.StartCursor = httpQuery.Get("start_cursor")
		}
		if httpQuery.Has("country") {
			query.Country = httpQuery.Get("country")
		}
		result, err := handler.Handle(ctx, query)
		if err != nil {
			hlog.FromRequest(req).Error().Err(err).Msg("something wrong during getting users")
			produceInternalServerError(ctx, w)
			return
		}

		items := make([]userResponse, len(result.Paginated.Users))
		for i, user := range result.Paginated.Users {
			items[i] = mapUserToResponse(user)
		}

		produceResponse(ctx, w, http.StatusOK, response{
			HasMore:    result.Paginated.HasNextPage(),
			NextCursor: result.Paginated.NextPageCursor,
			Items:      items,
		})
		return
	}
}

func NewGetUserByIDHTTPHandler(handler *users.GetUserByIDQueryHandler) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		ctx := req.Context()
		vars := mux.Vars(req)
		userID, ok := vars["id"]
		if !ok {
			hlog.FromRequest(req).Error().Str("uri", req.RequestURI).Msg("missing user id in request")
			produceInternalServerError(ctx, w)
			return
		}
		query := users.GetUserByIDQuery{ID: userID}
		user, err := handler.Handle(ctx, query)
		if err != nil {
			notFoundErr := users.NotFoundError{}
			if errors.As(err, &notFoundErr) {
				produceResponse(ctx, w, http.StatusNotFound, ErrorResp{
					Type:    "not_found",
					Message: "User with given id not found",
				})
				return
			}
			hlog.FromRequest(req).Error().Err(err).Str("user_id", userID).Msg("error during getting user by id")
			produceInternalServerError(ctx, w)
			return
		}
		produceResponse(ctx, w, http.StatusOK, mapUserToResponse(user))
		return
	}
}

func NewCreateUserHTTPHandler(handler *users.CreateUserCommandHandler) http.HandlerFunc {
	type createUserBody struct {
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
		Nickname  string `json:"nickname"`
		Password  string `json:"password"`
		Email     string `json:"email"`
		Country   string `json:"country"`
	}
	type resp struct {
		ID string `json:"id"`
	}
	return func(w http.ResponseWriter, req *http.Request) {
		logger := hlog.FromRequest(req)
		body := createUserBody{}

		decoder := json.NewDecoder(req.Body)
		err := decoder.Decode(&body)
		if err != nil {
			logger.Error().Err(err).Msg("can't decode body")
			produceResponse(req.Context(), w, http.StatusBadRequest, ErrorResp{
				Type:    "bad_request",
				Message: "Invalid Body",
			})
			return
		}
		ctx := req.Context()

		result, err := handler.Handle(ctx, users.CreateUserCommand{Data: users.CreateUserData{
			FirstName: body.FirstName,
			LastName:  body.LastName,
			Nickname:  body.Nickname,
			Password:  body.Password,
			Email:     body.Email,
			Country:   body.Country,
		}})
		if err != nil {
			validationError := users.ValidationError{}
			if errors.As(err, &validationError) {
				produceResponse(ctx, w, http.StatusBadRequest, ErrorResp{
					Type:    "validation_error",
					Message: validationError.Msg,
				})
				return
			}

			logger.Error().Err(err).Msg("error during user creation")
			produceInternalServerError(ctx, w)
			return
		}

		produceResponse(req.Context(), w, http.StatusCreated, resp{
			ID: result.UserID,
		})
	}
}
