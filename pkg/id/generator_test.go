package id

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGenerator_Generate(t *testing.T) {
	t.Parallel()

	generatedIDs := map[string]struct{}{}
	testIdsQuantity := 100
	idGen := NewGenerator()

	for i := 0; i < testIdsQuantity; i++ {
		newID := idGen.Generate(context.Background())
		_, ok := generatedIDs[newID]

		assert.False(t, ok, "generated it should be unique")

		generatedIDs[newID] = struct{}{}
	}
}
