package id

import (
	"context"

	"github.com/rs/xid"
)

type Generator struct {
}

func NewGenerator() *Generator {
	return &Generator{}
}

func (g *Generator) Generate(_ context.Context) string {
	guid := xid.New()
	return guid.String()
}
