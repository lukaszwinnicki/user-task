package mysql

import (
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql" // using mysql - this comment is created to silence linter
	"github.com/jmoiron/sqlx"
)

type Configuration struct {
	Addr            string
	DB              string
	User            string
	Password        string
	MaxOpenConns    int
	MaxIdleConns    int
	ConnMaxLifetime time.Duration
}

type Connection struct {
	db *sqlx.DB
}

func NewConnection(conf Configuration) (*Connection, error) {
	conn := &Connection{}
	connStr := fmt.Sprintf(
		"%s:%s@tcp(%s)/%s?collation=utf8mb4_unicode_ci&parseTime=true",
		conf.User,
		conf.Password,
		conf.Addr,
		conf.DB,
	)
	db, err := sqlx.Connect("mysql", connStr)
	if err != nil {
		return conn, fmt.Errorf("couldn't connect to mysql `%s`: %w", connStr, err)
	}
	db.SetConnMaxLifetime(conf.ConnMaxLifetime)
	db.SetMaxIdleConns(conf.MaxIdleConns)
	db.SetMaxOpenConns(conf.MaxOpenConns)

	conn.db = db

	return conn, nil
}
