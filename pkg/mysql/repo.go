package mysql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/rs/zerolog"

	"gitlab.com/lukaszwinnicki/user-task/pkg/users"
)

var createTableSQL = `
CREATE TABLE IF NOT EXISTS users(
id CHAR(20) PRIMARY KEY,
first_name VARCHAR(255) not null,
last_name VARCHAR(255) not null, 
nickname VARCHAR(255) not null, 
password varchar(255) not null,
email varchar(255) not null unique,
country varchar(255) not null,
created_at datetime not null,
updated_at datetime not null
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
`

type userDB struct {
	ID        string    `db:"id"`
	FirstName string    `db:"first_name"`
	LastName  string    `db:"last_name"`
	Nickname  string    `db:"nickname"`
	Password  string    `db:"password"` // TODO salt it
	Email     string    `db:"email"`
	Country   string    `db:"country"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
}

type UserRepository struct {
	conn *Connection
}

func NewUserRepository(ctx context.Context, conn *Connection, createTable bool) (*UserRepository, error) {
	repo := &UserRepository{
		conn: conn,
	}
	if createTable {
		err := repo.createTable(ctx)
		if err != nil {
			return nil, fmt.Errorf("something wrong during creation users table: %w", err)
		}
	}
	return &UserRepository{
		conn: conn,
	}, nil
}

func (u *UserRepository) Save(ctx context.Context, user users.User) error {
	query := `
INSERT INTO users(id, first_name, last_name, nickname, password, email, country, created_at, updated_at)
VALUES (:id, :first_name, :last_name, :nickname, :password, :email, :country, :created_at, :updated_at)
`
	_, err := u.conn.db.NamedExecContext(ctx, query, map[string]any{
		"id":         user.ID,
		"first_name": user.FirstName,
		"last_name":  user.LastName,
		"nickname":   user.Nickname,
		"password":   user.Password,
		"email":      user.Email,
		"country":    user.Country,
		"created_at": user.CreatedAt,
		"updated_at": user.UpdatedAt,
	})
	if err != nil {
		return fmt.Errorf("can't save a user with id '%s' and email '%s' in DB: %w", user.ID, user.Email, err)
	}
	return nil
}

func (u *UserRepository) Remove(ctx context.Context, id string) error {
	_, err := u.conn.db.NamedExecContext(ctx, "DELETE FROM users WHERE id = :id LIMIT 1", map[string]any{"id": id})
	if err != nil {
		return fmt.Errorf("can't delete user from db with id '%s': %w", id, err)
	}

	return nil
}

func (u *UserRepository) HasUserWithEmail(ctx context.Context, email string) (bool, error) {
	var id string
	err := u.conn.db.QueryRowContext(ctx, "SELECT id FROM users WHERE email = ? limit 1", email).Scan(&id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, nil
		}
		return false, fmt.Errorf("can't make query searching user by email '%s': %w", email, err)
	}
	return true, nil
}

func (u *UserRepository) GetUserByID(ctx context.Context, id string) (users.User, error) {
	var uDB userDB
	rows, err := u.conn.db.NamedQueryContext(ctx, "SELECT * FROM users WHERE id=:id LIMIT 1", map[string]any{"id": id})
	if err != nil {
		return users.User{}, fmt.Errorf("can't make query to get user by id '%s': %w", id, err)
	}
	rowExists := rows.Next()
	if !rowExists {
		return users.User{}, users.NotFoundError{Msg: fmt.Sprintf("user with id '%s' not found", id)}
	}
	err = rows.StructScan(&uDB)
	if err != nil {
		return users.User{}, fmt.Errorf("something wrong during scanning user with id '%s': %w", id, err)
	}
	rows.Close()

	return u.MapUserDBtoUser(uDB), nil
}

func (u *UserRepository) GetUsersPaginated(
	ctx context.Context,
	filter users.GetUsersFilter,
	startCursor string,
	pageSize int,
) (users.Paginated, error) {
	limitForPaging := pageSize + 1 // we are getting one row more to get value for next page cursor
	queryArgs := map[string]any{
		"limit":        limitForPaging,
		"start_cursor": startCursor,
	}
	searchColumns := []string{"id >= :start_cursor"}
	query := "SELECT * FROM users"
	if filter.Country != "" {
		queryArgs["country"] = filter.Country
		searchColumns = append(searchColumns, "country = :country")
	}
	if len(searchColumns) > 0 {
		query = fmt.Sprintf("%s WHERE", query)
		query = fmt.Sprintf("%s %s", query, strings.Join(searchColumns, " AND "))
	}
	query = fmt.Sprintf("%s ORDER BY id ASC LIMIT :limit", query)
	rows, err := u.conn.db.NamedQueryContext(ctx, query, queryArgs)
	if err != nil {
		return users.Paginated{}, fmt.Errorf("can't make query to get users: %w", err)
	}

	usersItems := []users.User{}
	for rows.Next() {
		uDB := userDB{}
		err = rows.StructScan(&uDB)
		if err != nil {
			return users.Paginated{}, fmt.Errorf("something wrong during scanning user: %w", err)
		}
		usersItems = append(usersItems, u.MapUserDBtoUser(uDB))
	}
	nextCursor := ""
	if len(usersItems) == limitForPaging {
		nextCursor = usersItems[limitForPaging-1].ID // next page cursor is an id of the last, not returned, item
		usersItems = usersItems[:limitForPaging-1]
	}

	return users.Paginated{Users: usersItems, NextPageCursor: nextCursor}, nil
}

func (u *UserRepository) MapUserDBtoUser(uDB userDB) users.User {
	return users.User{
		ID:        uDB.ID,
		FirstName: uDB.FirstName,
		LastName:  uDB.LastName,
		Nickname:  uDB.Nickname,
		Password:  uDB.Password,
		Email:     uDB.Email,
		Country:   uDB.Country,
		CreatedAt: uDB.CreatedAt,
		UpdatedAt: uDB.UpdatedAt,
	}
}

func (u *UserRepository) createTable(ctx context.Context) error {
	_, err := u.conn.db.Exec(createTableSQL)
	if err != nil {
		return fmt.Errorf("can't create users table: %w", err)
	}
	zerolog.Ctx(ctx).Info().Msg("created table 'users'")
	return nil
}
