package rabbit

import (
	"context"

	"github.com/rs/zerolog"

	"gitlab.com/lukaszwinnicki/user-task/pkg/users"
)

// EventsListener in real application should send messages to rabbit.
// In case where we should do more operations after user creation, we can create new listener with injected array of
// listeners and calling those one after another.
type EventsListener struct {
}

func NewEventsListener() *EventsListener {
	return &EventsListener{}
}

func (l *EventsListener) UserCreated(ctx context.Context, event users.UserCreatedEvent) {
	zerolog.Ctx(ctx).Info().Str("new_user_id", event.UserID).Msg("sending message to rabbitMQ about new user")
}

func (l *EventsListener) UserDeleted(ctx context.Context, event users.UserDeletedEvent) {
	zerolog.Ctx(ctx).Info().Str("deleted_user_id", event.UserID).Msg("sending message to rabbitMQ about deleted user")
}
