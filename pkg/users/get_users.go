package users

import (
	"context"
	"fmt"
)

type GetUsersQuery struct {
	StartCursor string
	Country     string
}

type GetUsersQueryHandler struct {
	repo UserRepository
}

type GetUsersResult struct {
	Paginated Paginated
}

type GetUsersFilter struct {
	Country string
}

func NewGetUsersQueryHandler(repo UserRepository) *GetUsersQueryHandler {
	return &GetUsersQueryHandler{repo: repo}
}

func (h *GetUsersQueryHandler) Handle(ctx context.Context, query GetUsersQuery) (GetUsersResult, error) {
	pageSize := 10 // TODO: should get it from query
	filter := GetUsersFilter{}
	if query.Country != "" {
		filter.Country = query.Country
	}
	paginatedUsers, err := h.repo.GetUsersPaginated(ctx, filter, query.StartCursor, pageSize)
	if err != nil {
		return GetUsersResult{}, fmt.Errorf("can't get Users from repo: %w", err)
	}
	res := GetUsersResult{Paginated: paginatedUsers}

	return res, nil
}
