package users

import (
	"context"
	"fmt"
)

type GetUserByIDQuery struct {
	ID string
}

type GetUserByIDQueryHandler struct {
	repo UserRepository
}

func NewGetUserByIDQueryHandler(repo UserRepository) *GetUserByIDQueryHandler {
	return &GetUserByIDQueryHandler{repo: repo}
}

func (h *GetUserByIDQueryHandler) Handle(ctx context.Context, query GetUserByIDQuery) (User, error) {
	user, err := h.repo.GetUserByID(ctx, query.ID)
	if err != nil {
		return user, fmt.Errorf("can't get user by id from repository: %w", err)
	}
	return user, nil
}
