package users

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

type UserFactoryMock struct {
	toReturnUser User
}

func (f *UserFactoryMock) CreateUser(_ context.Context, _ CreateUserData) (User, error) {
	return f.toReturnUser, nil
}

type UserPersisterMock struct {
	receivedUser User
}

func (p *UserPersisterMock) Remove(_ context.Context, _ string) error {
	//TODO implement me
	panic("implement me")
}

func (p *UserPersisterMock) Save(_ context.Context, user User) error {
	p.receivedUser = user
	return nil
}

type UserRepositoryMock struct {
	receivedEmail               string
	toReturnUserWithEmailExists bool
}

func (r *UserRepositoryMock) GetUserByID(_ context.Context, _ string) (User, error) {
	//TODO implement me
	panic("implement me")
}

func (r *UserRepositoryMock) GetUsersPaginated(_ context.Context, _ GetUsersFilter, _ string, _ int) (Paginated, error) {
	//TODO implement me
	panic("implement me")
}

func (r *UserRepositoryMock) HasUserWithEmail(_ context.Context, email string) (bool, error) {
	r.receivedEmail = email
	return r.toReturnUserWithEmailExists, nil
}

type UserCreatedListenerMock struct {
	receivedEvent UserCreatedEvent
}

func (l *UserCreatedListenerMock) UserCreated(_ context.Context, event UserCreatedEvent) {
	l.receivedEvent = event
}

func TestCreateUserCommandHandler_Handle(t *testing.T) {
	t.Run("it should create user", func(t *testing.T) {
		repo := &UserRepositoryMock{toReturnUserWithEmailExists: false}
		persister := &UserPersisterMock{}
		newUserID := "new-user-id"
		newUserEmail := "new-user-email"
		newUser := User{ID: newUserID}
		factory := &UserFactoryMock{toReturnUser: newUser}
		listener := &UserCreatedListenerMock{}
		handler := NewCreateUserCommandHandler(repo, persister, factory, listener)
		data := CreateUserData{
			Email: newUserEmail,
		}

		result, err := handler.Handle(context.Background(), CreateUserCommand{Data: data})

		assert.NoError(t, err, "it should create user without error")
		assert.Equal(t, newUserID, result.UserID, "it should return ID of the new created user")
		assert.Equal(t, newUserEmail, repo.receivedEmail, "it should call repo to check email uniqueness")
		assert.Equal(t, newUser, persister.receivedUser, "it should receive user created by factory")
		assert.Equal(t, newUserID, listener.receivedEvent.UserID, "it should receive new user id")
	})

	t.Run("it should return error if user already exists with given email", func(t *testing.T) {
		repo := &UserRepositoryMock{toReturnUserWithEmailExists: true}
		persister := &UserPersisterMock{}
		factory := &UserFactoryMock{}
		listener := &UserCreatedListenerMock{}
		newUserEmail := "new-user-email-already-used"
		handler := NewCreateUserCommandHandler(repo, persister, factory, listener)
		data := CreateUserData{
			Email: newUserEmail,
		}

		_, err := handler.Handle(context.Background(), CreateUserCommand{Data: data})

		assert.Error(t, err, "it should receiver error if user with given email already exist")
		validationErr := ValidationError{}
		assert.True(
			t,
			errors.As(err, &validationErr),
			"it should return validation error if user with given email already exists %+v", err,
		)
	})
}
