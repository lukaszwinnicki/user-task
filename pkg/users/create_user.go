package users

import (
	"context"
	"fmt"
	"github.com/rs/zerolog"
)

type UserFactory interface {
	CreateUser(ctx context.Context, data CreateUserData) (User, error)
}

type UserCreatedListener interface {
	UserCreated(ctx context.Context, event UserCreatedEvent)
}

type UserCreatedEvent struct {
	UserID string
}

type CreateUserCommand struct {
	Data CreateUserData
}

type CreateUserCommandHandler struct {
	repo      UserRepository
	persister UserPersister
	factory   UserFactory
	listener  UserCreatedListener
}

type CreateUserResult struct {
	UserID string
}

func NewCreateUserCommandHandler(
	repo UserRepository,
	persister UserPersister,
	factory UserFactory,
	listener UserCreatedListener,
) *CreateUserCommandHandler {
	return &CreateUserCommandHandler{
		repo:      repo,
		persister: persister,
		factory:   factory,
		listener:  listener,
	}
}

func (h *CreateUserCommandHandler) Handle(ctx context.Context, command CreateUserCommand) (CreateUserResult, error) {
	result := CreateUserResult{}
	user, err := h.factory.CreateUser(ctx, command.Data)
	if err != nil {
		return result, fmt.Errorf("something wrong during user creation: %w", err)
	}
	exists, err := h.repo.HasUserWithEmail(ctx, command.Data.Email)
	if err != nil {
		return result, fmt.Errorf("something wrong during checking user's email: %w", err)
	}
	if exists {
		return result, ValidationError{Msg: fmt.Sprintf("user with email '%s' already exists", command.Data.Email)}
	}
	err = h.persister.Save(ctx, user)
	if err != nil {
		return result, fmt.Errorf("can't save new user: %w", err)
	}
	zerolog.Ctx(ctx).Info().Str("user_id", user.ID).Msg("new user created")
	h.listener.UserCreated(ctx, UserCreatedEvent{UserID: user.ID})
	result.UserID = user.ID

	return result, nil
}
