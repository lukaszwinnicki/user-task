package users

import (
	"context"
	"errors"
	"fmt"
	"github.com/rs/zerolog"
)

type DeleteUserCommand struct {
	UserID string
}

type UserDeletedEvent struct {
	UserID string
}

type UserDeletedListener interface {
	UserDeleted(ctx context.Context, event UserDeletedEvent)
}

type DeleteUserCommandHandler struct {
	repo      UserRepository
	persister UserPersister
	listener  UserDeletedListener
}

func NewDeleteUserCommandHandler(
	repo UserRepository,
	persister UserPersister,
	listener UserDeletedListener,
) *DeleteUserCommandHandler {
	return &DeleteUserCommandHandler{
		repo:      repo,
		persister: persister,
		listener:  listener,
	}
}

func (h *DeleteUserCommandHandler) Handle(ctx context.Context, command DeleteUserCommand) error {
	userID := command.UserID
	_, err := h.repo.GetUserByID(ctx, userID)
	userExists := true
	if err != nil {
		notFoundErr := NotFoundError{}
		if !errors.As(err, &notFoundErr) {
			return fmt.Errorf("something wrong happer during getting user for deletion: %w", err)
		}
		userExists = false
	}
	if !userExists {
		return nil
	}

	err = h.persister.Remove(ctx, userID)
	if err != nil {
		return fmt.Errorf("error during user deletion: %w", err)
	}
	zerolog.Ctx(ctx).Info().Str("user_id", userID).Msg("user deleted")
	h.listener.UserDeleted(ctx, UserDeletedEvent{UserID: userID})

	return nil
}
