package users

import (
	"context"
	"time"
)

type ValidationError struct {
	Msg string
}

func (v ValidationError) Error() string {
	return v.Msg
}

type NotFoundError struct {
	Msg string
}

func (v NotFoundError) Error() string {
	return v.Msg
}

type User struct {
	ID        string
	FirstName string
	LastName  string
	Nickname  string
	Password  string // TODO salt it
	Email     string
	Country   string
	CreatedAt time.Time
	UpdatedAt time.Time
}

type CreateUserData struct {
	FirstName string
	LastName  string
	Nickname  string
	Password  string // TODO salt it
	Email     string
	Country   string
}

type IDGenerator interface {
	Generate(ctx context.Context) string
}

type Clock interface {
	Now() time.Time
}

type Paginated struct {
	Users          []User
	NextPageCursor string
}

func (p Paginated) HasNextPage() bool {
	return p.NextPageCursor != ""
}

type UserRepository interface {
	HasUserWithEmail(ctx context.Context, email string) (bool, error)
	// GetUserByID returns NotFoundError if user can't be found.
	GetUserByID(ctx context.Context, id string) (User, error)
	GetUsersPaginated(ctx context.Context, filter GetUsersFilter, startCursor string, pageSize int) (Paginated, error)
}

type UserPersister interface {
	Save(ctx context.Context, user User) error
	Remove(ctx context.Context, id string) error
}

type DefaultUserFactory struct {
	idGenerator IDGenerator
	clock       Clock
}

func NewUserFactory(idGenerator IDGenerator, clock Clock) *DefaultUserFactory {
	return &DefaultUserFactory{idGenerator: idGenerator, clock: clock}
}

// CreateUser is responsible for creating valid User instance.
// It returns ValidationError if given Data is invalid.
func (f *DefaultUserFactory) CreateUser(ctx context.Context, data CreateUserData) (User, error) {
	u := User{}
	// here should land different kind of validations
	if data.Email == "" {
		return u, ValidationError{Msg: "empty email"}
	}
	if data.Password == "" {
		return u, ValidationError{Msg: "empty password"}
	}

	u.ID = f.idGenerator.Generate(ctx)
	u.FirstName = data.FirstName
	u.LastName = data.LastName
	u.Nickname = data.Nickname
	u.Password = data.Password
	u.Email = data.Email
	u.Country = data.Country
	now := f.clock.Now()
	u.CreatedAt = now
	u.UpdatedAt = now

	return u, nil
}
