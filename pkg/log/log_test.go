package log

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/hlog"
	"github.com/stretchr/testify/assert"

	"gitlab.com/lukaszwinnicki/user-task/pkg/id"
)

func prepareStackForMiddlewareTest(
	logProducerHandler http.Handler,
	testedMiddleware func(handler http.Handler) http.Handler,
) (http.Handler, io.Reader) {
	buf := bytes.NewBuffer([]byte{})
	logger := zerolog.New(buf).With().Logger()
	loggerMiddleware := hlog.NewHandler(logger)

	h := loggerMiddleware(testedMiddleware(logProducerHandler))

	return h, buf
}

func unmarshalLog(t *testing.T, reader io.Reader, unmarshalInto interface{}) {
	logContent, err := ioutil.ReadAll(reader)
	if err != nil {
		t.Fatal("can't read log content", err)
	}
	err = json.Unmarshal(logContent, unmarshalInto)
	if err != nil {
		t.Fatal("can't unmarshal log content", err)
	}
}

func TestMiddlewareRequest(t *testing.T) {
	t.Parallel()
	h, reader := prepareStackForMiddlewareTest(
		http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			hlog.FromRequest(req).Info().Msg("some message")
			w.WriteHeader(http.StatusNoContent)
		}),
		MiddlewareDecorateLogger(id.NewGenerator()),
	)

	testCases := []struct {
		name         string
		buildRequest func() *http.Request
		assertLog    func(t *testing.T, producedLog map[string]string)
	}{
		{
			name: "it should log generated requests id along with method and URL",
			buildRequest: func() *http.Request {
				return httptest.NewRequest(http.MethodGet, "/some-path", nil)
			},
			assertLog: func(t *testing.T, producedLog map[string]string) {
				_, hasRequestID := producedLog["req_id"]
				assert.True(t, hasRequestID)
				assert.Equal(t, http.MethodGet, producedLog["method"])
				assert.Equal(t, "/some-path", producedLog["url"])
			},
		},
		{
			name: "it should log request id from `X-Request-Id` header",
			buildRequest: func() *http.Request {
				req := httptest.NewRequest(http.MethodGet, "/some-path", nil)
				req.Header["X-Request-Id"] = []string{"request-id-from-header"}

				return req
			},
			assertLog: func(t *testing.T, producedLog map[string]string) {
				assert.Equal(t, "request-id-from-header", producedLog["req_id"])
				assert.Equal(t, http.MethodGet, producedLog["method"])
				assert.Equal(t, "/some-path", producedLog["url"])
			},
		},
	}

	for _, testCase := range testCases {
		testCase := testCase
		t.Run(testCase.name, func(t *testing.T) {
			w := httptest.NewRecorder()
			req := testCase.buildRequest()

			h.ServeHTTP(w, req)

			log := make(map[string]string)
			unmarshalLog(t, reader, &log)
			testCase.assertLog(t, log)
		})
	}

}

func TestMiddlewareLogRequest(t *testing.T) {
	t.Parallel()
	type logSchema struct {
		Level  string `json:"level"`
		Method string `json:"method"`
		URL    string `json:"url"`
		Status int    `json:"status"`
	}
	testCases := []struct {
		name               string
		returnedStatusCode int
		expectedLogLevel   string
	}{
		{
			name:               "it should log debug for returned 2** status code",
			returnedStatusCode: http.StatusOK,
			expectedLogLevel:   "debug",
		},
		{
			name:               "it should log debug for returned 3** status code",
			returnedStatusCode: http.StatusTemporaryRedirect,
			expectedLogLevel:   "debug",
		},
		{
			name:               "it should log info for returned 4** status code",
			returnedStatusCode: http.StatusBadRequest,
			expectedLogLevel:   "info",
		},
		{
			name:               "it should log info for returned 5** status code",
			returnedStatusCode: http.StatusInternalServerError,
			expectedLogLevel:   "info",
		},
	}

	for _, testCase := range testCases {
		testCase := testCase
		t.Run(testCase.name, func(t *testing.T) {
			h, reader := prepareStackForMiddlewareTest(
				http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
					w.WriteHeader(testCase.returnedStatusCode)
				}),
				MiddlewareLogRequest(),
			)
			req := httptest.NewRequest(http.MethodGet, "/some-path", nil)
			w := httptest.NewRecorder()

			h.ServeHTTP(w, req)

			log := logSchema{}
			unmarshalLog(t, reader, &log)
			assert.Equal(t, testCase.expectedLogLevel, log.Level)
			assert.Equal(t, testCase.returnedStatusCode, log.Status)
		})
	}
}
