package log

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/hlog"
)

type reqIDGenerator interface {
	Generate(ctx context.Context) string
}

func ConfigureLogger(logLevelFromConfig string) (zerolog.Logger, error) {
	logLevel, err := zerolog.ParseLevel(logLevelFromConfig)
	if err != nil {
		return zerolog.Logger{}, fmt.Errorf(
			"can't parse given log level: given - %s, err - %w",
			logLevelFromConfig,
			err,
		)
	}
	zerolog.SetGlobalLevel(logLevel)
	return zerolog.New(os.Stdout).With().Caller().Timestamp().Logger(), nil
}

func MiddlewareDecorateLogger(idGenerator reqIDGenerator) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			requestID := req.Header.Get("X-Request-Id")
			if requestID == "" {
				requestID = idGenerator.Generate(req.Context())
			}
			logger := hlog.FromRequest(req).With().
				Str("req_id", requestID).
				Str("method", req.Method).
				Stringer("url", req.URL).
				Logger()

			req = req.WithContext(logger.WithContext(req.Context()))
			next.ServeHTTP(w, req)
		})
	}
}

func MiddlewareLogRequest() func(http.Handler) http.Handler {
	return hlog.AccessHandler(func(req *http.Request, status, size int, duration time.Duration) {
		logger := hlog.FromRequest(req).
			With().
			Int("status", status).
			Int64("duration_ms", duration.Milliseconds()).
			Logger()

		if status >= 400 {
			logger.Info().Msg("")
		} else { //richer logs on debug
			logger.Debug().Msg("")
		}
	})
}
