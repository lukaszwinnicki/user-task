package config

import (
	"fmt"
	"os"
	"strconv"
	"time"
)

type Configuration struct {
	Port     int
	LogLevel string
	Mysql    struct {
		InitDB          bool
		Addr            string
		Db              string
		User            string
		Password        string
		MaxOpenConns    int
		MaxIdleConns    int
		ConnMaxLifetime time.Duration
	}
}

func GetConfigurationFromEnv() (Configuration, error) {
	conf := Configuration{}
	port, err := getEnvVarInt("PORT")
	if err != nil {
		return conf, err
	}
	conf.Port = port
	logLevel, err := getEnvVar("LOG_LEVEL")
	if err != nil {
		return conf, err
	}
	conf.LogLevel = logLevel
	mysqlInitDB, err := getEnvVarBool("MYSQL_INIT_DB")
	if err != nil {
		return conf, err
	}
	mysqlAddr, err := getEnvVar("MYSQL_ADDR")
	if err != nil {
		return conf, err
	}
	mysqlDB, err := getEnvVar("MYSQL_DATABASE")
	if err != nil {
		return conf, err
	}
	mysqlUser, err := getEnvVar("MYSQL_USER")
	if err != nil {
		return conf, err
	}
	mysqlPassword, err := getEnvVar("MYSQL_PASSWORD")
	if err != nil {
		return conf, err
	}
	mysqlMaxOpenConns, err := getEnvVarInt("MYSQL_MAX_OPEN_CONNS")
	if err != nil {
		return conf, err
	}
	mysqlMaxIdleConns, err := getEnvVarInt("MYSQL_MAX_IDLE_CONNS")
	if err != nil {
		return conf, err
	}
	mysqlConnMaxLifetime, err := getEnvVarDuration("MYSQL_CONN_MAX_LIFETIME")
	if err != nil {
		return conf, err
	}
	conf.Mysql.InitDB = mysqlInitDB
	conf.Mysql.Addr = mysqlAddr
	conf.Mysql.Db = mysqlDB
	conf.Mysql.User = mysqlUser
	conf.Mysql.Password = mysqlPassword
	conf.Mysql.MaxOpenConns = mysqlMaxOpenConns
	conf.Mysql.MaxIdleConns = mysqlMaxIdleConns
	conf.Mysql.ConnMaxLifetime = mysqlConnMaxLifetime

	return conf, nil
}

func getEnvVar(name string) (string, error) {
	val := os.Getenv(name)
	if len(val) < 1 {
		return val, fmt.Errorf("missing '%s'", name)
	}
	return val, nil
}

func getEnvVarInt(name string) (int, error) {
	valRaw, err := getEnvVar(name)
	if err != nil {
		return 0, err
	}
	val, err := strconv.Atoi(valRaw)
	if err != nil {
		return 0, fmt.Errorf("can't parse given env '%s' to int: %w", valRaw, err)
	}
	return val, nil
}

func getEnvVarDuration(name string) (time.Duration, error) {
	valRaw, err := getEnvVar(name)
	if err != nil {
		return 0, err
	}
	val, err := time.ParseDuration(valRaw)
	if err != nil {
		return 0, fmt.Errorf("can't parse given env '%s' to duration: %w", valRaw, err)
	}
	return val, nil
}

func getEnvVarBool(name string) (bool, error) {
	valRaw, err := getEnvVar(name)
	if err != nil {
		return false, err
	}
	val, err := strconv.ParseBool(valRaw)
	if err != nil {
		return false, fmt.Errorf("can't parse given env '%s' to bool: %w", valRaw, err)
	}
	return val, nil
}
